enum Estado { vivo, muerto }

interface Persona {
    nombre: string;
    familia: string;
    edad: number;
    estado: Estado;

    comunicar(): void;
}

class Luchador implements Persona {

    private destreza0_10: number;

    constructor(public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estado,
        public arma: string
    ) { }

    set destreza(d: number) {
        this.destreza0_10 = d >= 0 && d <= 10 ? d : 5;
    }

    get destreza(): number {
        return this.destreza0_10;
    }

    public comunicar(): void {
        console.log('Primero pego y luego pregunto');
    }

}

class Asesor implements Persona {

    constructor(public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estado,
        public asesorado: Persona
    ) { }

    public comunicar(): void {
        console.log('No sé por qué, pero creo que voy a morir pronto');
    }

}

class Escudero implements Persona {

    constructor(public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estado,
        private pelotismo: number,
        public servido: Luchador
    ) { }

    public comunicar(): void {
        console.log(`El arma de mi amo es ${this.servido.arma}`);
    }
}

let jamie = new Luchador('Jamie', 'Lannister', 30, Estado.vivo, 'Espada');
let daenerys = new Luchador('Daenerys', 'Targaryen', 28, Estado.vivo, 'Dragones');
let tyrion = new Asesor('Tyrion', 'Lannister', 40, Estado.vivo, daenerys);
let bronn = new Escudero('Bronn', null, 40, Estado.vivo, 4, jamie);


let personajes: Persona[] = [jamie, daenerys, tyrion, bronn];

// personajes.forEach(personaje => personaje.comunicar());

for (let personaje of personajes) {
    personaje.comunicar();
}
