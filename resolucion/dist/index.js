var Estado;
(function (Estado) {
    Estado[Estado["vivo"] = 0] = "vivo";
    Estado[Estado["muerto"] = 1] = "muerto";
})(Estado || (Estado = {}));
var Luchador = /** @class */ (function () {
    function Luchador(nombre, familia, edad, estado, arma) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.arma = arma;
    }
    Object.defineProperty(Luchador.prototype, "destreza", {
        get: function () {
            return this.destreza0_10;
        },
        set: function (d) {
            this.destreza0_10 = d >= 0 && d <= 10 ? d : 5;
        },
        enumerable: true,
        configurable: true
    });
    Luchador.prototype.comunicar = function () {
        console.log('Primero pego y luego pregunto');
    };
    return Luchador;
}());
var Asesor = /** @class */ (function () {
    function Asesor(nombre, familia, edad, estado, asesorado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.asesorado = asesorado;
    }
    Asesor.prototype.comunicar = function () {
        console.log('No sé por qué, pero creo que voy a morir pronto');
    };
    return Asesor;
}());
var Escudero = /** @class */ (function () {
    function Escudero(nombre, familia, edad, estado, pelotismo, servido) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.pelotismo = pelotismo;
        this.servido = servido;
    }
    Escudero.prototype.comunicar = function () {
        console.log("El arma de mi amo es " + this.servido.arma);
    };
    return Escudero;
}());
var jamie = new Luchador('Jamie', 'Lannister', 30, Estado.vivo, 'Espada');
var daenerys = new Luchador('Daenerys', 'Targaryen', 28, Estado.vivo, 'Dragones');
var tyrion = new Asesor('Tyrion', 'Lannister', 40, Estado.vivo, daenerys);
var bronn = new Escudero('Bronn', null, 40, Estado.vivo, 4, jamie);
var personajes = [jamie, daenerys, tyrion, bronn];
// personajes.forEach(personaje => personaje.comunicar());
for (var _i = 0, personajes_1 = personajes; _i < personajes_1.length; _i++) {
    var personaje = personajes_1[_i];
    personaje.comunicar();
}
//# sourceMappingURL=index.js.map